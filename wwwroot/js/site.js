﻿
/* Start of Navbar animation */
// Used to shrink navbar removing paddings and adding black background
   
        $(window).scroll(function () {
                if ($(document).scrollTop() > 50) {
            $('.nav').addClass('affix');
                    console.log("OK");
                } else {
            $('.nav').removeClass('affix');
                }
            });

        // Makes the navbar stay
        //        $('.nav').addClass('affix');

/* End of Navbar animation */

/* Start of footer animation */
  
    $(document).ready(function () {

        // INITIATE THE FOOTER
        siteFooter();
           
            $(window).resize(function () {
        siteFooter();
            });

            function siteFooter() {
                var siteContent = $('#site-content');
                var siteContentHeight = siteContent.height();
                var siteContentWidth = siteContent.width();

                var siteFooter = $('#site-footer');
                var siteFooterHeight = siteFooter.height();
                var siteFooterWidth = siteFooter.width();

                console.log('Content Height = ' + siteContentHeight + 'px');
                console.log('Content Width = ' + siteContentWidth + 'px');
                console.log('Footer Height = ' + siteFooterHeight + 'px');
                console.log('Footer Width = ' + siteFooterWidth + 'px');

                siteContent.css({
        "margin-bottom": siteFooterHeight + 50
                });
            };
        });

/* End of footer animation */

/* Start of Scroll up animation */
 
var btn = $('#button');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 2300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '2300');
    });
    
/* End of Scroll up animation */


/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
/*
    <script>

        var dropdown = document.getElementsByClassName("dropbtn");
        var i;

            for (i = 0; i < dropdown.length; {
            dropdown[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                } else {
                    dropdownContent.style.display = "block";
                }
            });
            }
        </script>
 */

/* Start of gallery toggle animation */

$(".customSwitch").click((e) => {
    $(e.currentTarget).find(".toggle").toggleClass("toggleOn");
    $(e.currentTarget).find(".onLabel").toggle();
    $(e.currentTarget).find(".offLabel").toggle();

    if ($(e.currentTarget).val()) {
        $(e.currentTarget).val(false);
    } else {
        $(e.currentTarget).val(true);
    }
});

/* End of gallery toggle animation */

/* Start of gallery item scroll-in animation */

(function ($) {

    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *     the user visible viewport of a web browser.
     *     only accounts for vertical position, not horizontal.
     */

    $.fn.visible = function (partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

})(jQuery);

var win = $(window);

var allMods = $(".item");

allMods.each(function (i, el) {
    var el = $(el);
    if (el.visible(true)) {
        el.addClass("already-visible");
    }
});

win.scroll(function (event) {

    allMods.each(function (i, el) {
        var el = $(el);
        if (el.visible(true)) {
            el.addClass("come-in");
        }
    });

});

/* End of gallery item scroll-in animation */


